// import { NavigationNativeContainer } from '@react-navigation/native';
// import { createStackNavigator } from 'react-navigation-stack';
import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { Image } from 'react-native';
import home from '../screen/home';
import categories from '../screen/category';
import appicon from '../appicon/Home.png';
const AppNavigator = createMaterialBottomTabNavigator({
    Home: { screen: home,
      navigationOptions:{
           tabBarIcon: <Image source={appicon} style={{ width:24, height:24}}/>
        }
    },
    Category: { screen: categories,
      navigationOptions :{
        tabBarIcon: <Image source={require('../appicon/ic_nonactive_category.png')} style={{ width:22, height:22}}/>
      }
    },
    Order: {screen: home,
      navigationOptions :{
        tabBarIcon: <Image source={require('../appicon/Order.png')} style={{ width:24, height:24}}/>
      }
    },
    Wishlist: {screen: home,
      navigationOptions :{
        tabBarIcon: <Image source={require('../appicon/Wishlist.png')} style={{ width:24, height:24}}/>
      }
    },
    Account: {screen: home,
      navigationOptions :{
        tabBarIcon: <Image source={require('../appicon/ic_nonactive_account.png')} style={{ width:20, height:25}}/>
      }
    },
  },
  {
    initialRouteName: 'Home',
    activeColor: '#000000',
    inactiveColor: '#3e2465',
    barStyle: { backgroundColor: '#ffffff' },
  }
);
export default createAppContainer(AppNavigator);
