import React from 'react';
import { StyleSheet, Text, View, Image, TextInput } from 'react-native';
import spcart from '../appicon/cart.png';
import kinhlup from '../appicon/kinhlup.png';
export default class Category extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header} >
          <Image source={spcart} style={{ width:33, height:28, alignSelf:'flex-end'}}/>
          <View style={styles.SectionStyle}>
            <Image source={kinhlup} style={styles.ImageStyle} />
            <TextInput style={{ height: 48,  backgroundColor:'#FFFFFF', }} placeholder='search' />
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: 'rgba(247,247,247,1.0)',
    paddingTop: 50,
    flexDirection:'column',
  },
  header: {
    paddingHorizontal:10,
    height:100,
    flexDirection:'column',
    justifyContent:'flex-start',
  },
  ImageStyle: {
    padding: 10,
    marginHorizontal:10,
    height: 14,
    width: 14,
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 5,
    marginTop:10,
  },
});