/**
 * Jamadecor App
 * Home Screen
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {View, Text, StyleSheet, FlatList, SectionList, Image, TextInput, Dimensions, Platform } from 'react-native';
import spcart from '../appicon/cart.png';
import kinhlup from '../appicon/kinhlup.png';
import banner from '../appicon/Banner.png';
import kitchen from '../appicon/kitchent_icon.png';
import arrival from '../appicon/arrival.png';
import livingroom from '../appicon/livingroom.png';
import bedroom from '../appicon/bedroom.png';
import bathroom from '../appicon/bathroom.png';
import bestdeal from '../appicon/bestdeal.png';
import electronic from '../appicon/electronic.png';
import furniture from '../appicon/furniture.png';

const {cd} = Dimensions.get('window');
const ICON_SIZE = 48;
const FONT_SIZE = 12; 
const ICON_HEIGHT = 48;
const ICON_WIDTH = 48;
const getItem = (name_cate,image_cate) => (
  <View style={styles.iconStyle}>
    <Image source={image_cate} name="md-checkmark-circle" style={{width: ICON_WIDTH, height: ICON_HEIGHT}} color="green" />
    <Text style={styles.textStyle}>{name_cate}</Text>
  </View>
);
const data_list_pro = [
  {key: 'Devin'},
  {key: 'Dan'},
  {key: 'Dominic'},
  {key: 'Jackson'},
  {key: 'James'},
  {key: 'Joel'},
  {key: 'John'},
  {key: 'Jillian'},
  {key: 'Jimmy'},
  {key: 'Julie'},
];
const numColumns = 2;
export default class Home extends Component{
  renderItem = ({ item, index }) => {
    if (item.empty === true) {
      return <View style={[styles_proitem.item, styles.itemInvisible]} />;
    }
    return (
      <View style={styles_proitem.item}>
        <Image style={styles_proitem.images} source={require('../appicon/pro_2x.png')} />
        <Text style={styles_proitem.price_sold}>{item.key}</Text>
        <Text style={styles_proitem.price_pro}>{item.key}</Text>
       
      </View>
    );
  };
  render(){
    return(
      <View style={styles.container}>
        <View style={styles.header} >
          <Image source={spcart} style={{ width:33, height:28, alignSelf:'flex-end'}}/>
          <View style={styles.SectionStyle}>
            <Image source={kinhlup} style={styles.ImageStyle} />
            <TextInput style={{ height: 48,  backgroundColor:'#FFFFFF', }} placeholder='search' />
          </View>
        </View>
        <View style={styles.content}>
          <View style={styles.Banner}>
            <Image source={banner} style={{width: cd , height:160}}/>
          </View>
          {/* end banner */}

          <View style={styles.Category}>
            <View style={styles.titleHeaderSection}>
              <Text>Category</Text>
              <Text>Xem Tất Cả</Text>
            </View>
            <View style={styles.WrapCate1}>
              {getItem('Kitchen',kitchen)}
              {getItem('Living Room',livingroom)}
              {getItem('Bedroom',bedroom)}
              {getItem('Bathroom',bathroom)}
            </View>
            <View style={styles.WrapCate2}>
              {getItem('Furniture',furniture)}
              {getItem('Electronic & Gadget',electronic)}
              {getItem('New Arrivals',arrival)}
              {getItem('Best Deal',bestdeal)}
            </View>
          </View>
          {/* end category  */}

          <View style={styles.productsale}>
            <View style={styles.titleHeaderSection}>
              <Text>Special Sales</Text>
              <Text>Xem Tất Cả</Text>
            </View>
            <View style={{flex:1}}>
              <FlatList
                data= {data_list_pro}
                numColumns = {numColumns}
                renderItem={this.renderItem}
              />
            </View>
          </View>
          {/* end productsale */}

        </View>

        {/* end content */}
      </View>
    )
  }// render

}
const styles_proitem = StyleSheet.create({
  container: {
    flex: 1,
  },
  images: {
    width:170,
    height:170,
    flex:1,
  },
  item:{
    backgroundColor: '#ffffff',
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 4,
    height: Dimensions.get('window').width / numColumns, // gần đúng 1 hình vuông
  },
  price_sold: {
    textDecorationLine: 'line-through', 
    textDecorationStyle: 'solid',
    fontSize: 20,
  },
  price_pro: {
    color: '#FF9500',
    fontSize: 20,
    fontWeight: "bold",
  }
});
const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: 'rgba(247,247,247,1.0)',
    paddingTop: 50,
    flexDirection:'column',
  },
  header: {
    paddingHorizontal:10,
    height:100,
    flexDirection:'column',
    justifyContent:'flex-start',
  },
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 5,
    marginTop:10,
  },
  content:{
    paddingHorizontal:10,
    flex:1,
    flexDirection:'column',
  },
  Banner:{
    height:170,
  },
  Category:{
    flex:1,
    flexDirection:'column',
  },
  productsale:{
    flex:1,
  },
  sectionHeader: {
     paddingTop: 2,
     paddingLeft: 10,
     paddingRight: 10,
     paddingBottom: 2,
     fontSize: 14,
     fontWeight: 'bold',
     backgroundColor: 'rgba(247,247,247,1.0)',
   },

  titleHeaderSection:{
    flex:0.2, 
    paddingVertical:10, 
    flexDirection:"row", 
    justifyContent:'space-between',
  },

  item: {
     padding: 10,
     fontSize: 18,
     height: 44,
  },
   
  ImageStyle: {
      padding: 10,
      marginHorizontal:10,
      height: 14,
      width: 14,
      resizeMode: 'stretch',
      alignItems: 'center',
  },
 
  /* box text */
  WrapCate1: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection:'row',
  },
  WrapCate2:{
    flex: 1,
    justifyContent: 'space-between',
    flexDirection:'row',
  },
  iconContainer: {
    width: ICON_SIZE * 2,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  iconStyle: {
    flexDirection: 'column',
    alignItems: 'center',
    padding: 5,
  },
  textStyle: {
    fontSize: FONT_SIZE,
    flexShrink: 1,
    flex:1,
    flexGrow:1,
    flexDirection: 'column'
  },
  ...Platform.select({
    ios :{

    },
    android :{
      container: {
        paddingTop:10,
      },
      WrapCate1:{
        flex:1,
        justifyContent: 'space-between',
        flexDirection:'row',
        paddingBottom:100,
      },
      WrapCate2: {
        flex: 2,
        justifyContent: 'space-between',
        flexDirection:'row',
      },
      textStyle: {
        fontSize: FONT_SIZE,
        flexDirection: 'column'
      },
      titleHeaderSection:{
        flex:1, 
        paddingVertical:10, 
        flexDirection:"row", 
        justifyContent:'space-between',
      },
    }
  })

})
