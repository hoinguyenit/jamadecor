/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import HomeScreen from './screen/home';
import Navigator from './module_jama/Appnavigator';
const App: () => React$Node = () => {
  return (
    <Navigator/>
  );
};

export default App;
